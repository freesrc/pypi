#!/usr/bin/env bash
set -ex

mkdir -p "${CI_PROJECT_DIR}"/rebuild

for pkg in "${CI_PROJECT_DIR}"/output/build/*.whl;do
    _tmp=$(python3 -m wheel unpack --dest /tmp "${pkg}" | sed -n 's|^Unpacking to: \(.*\)\.\.\.OK|\1|p;');
    find "${_tmp}" -name '*METADATA' -print0 | xargs -I{} sed -i '/^$/q' '{}' ;
    python3 -m wheel pack --dest-dir "${CI_PROJECT_DIR}"/rebuild "${_tmp}";
    rm -rf /tmp/*
done

# python3 check-packages.py

python -m twine upload \
       --username gitlab-ci-token \
       --password "${CI_JOB_TOKEN}" \
       --skip-existing \
       --verbose \
       --repository-url "${CI_API_V4_URL}"/projects/"${CI_PROJECT_ID}"/packages/pypi \
       "${CI_PROJECT_DIR}"/rebuild/*
