ARG     ALPINE_VER=3.19
FROM    registry.gitlab.com/freesrc/docker/alpine:${ALPINE_VER}

ENV     MAKEFLAGS="-j$(nproc)"

COPY    packages.list /
RUN     set -eux ; \
        mkdir -p /build ; \
        apk add --no-cache --virtual .build-dep \
                bash sed gawk tar bzip2 \
                make cmake gcc g++ libstdc++ \
                python3 py3-pip linux-headers libc-utils \
                musl-dev python3-dev \
                openblas-dev lapack-dev libffi-dev \
                cython pythran pkgconf
RUN     set -eux ; \
        python3 -m pip install --no-cache-dir -U \
                --break-system-packages \
                twine setuptools numpy pybind11 wheel
RUN     set -eux ; \
        for pkg in $(cat /packages.list);do \
            python3 -m pip wheel \
                    --no-cache-dir \
                    --wheel-dir /build \
                    ${pkg} \
            ; \
        done
RUN     python3 -m pip uninstall -y \
                --break-system-packages \
                twine setuptools numpy pybind11 wheel ; \
        python3 -m pip uninstall -y \
                --break-system-packages \
                $(cat /packages.list) ; \
        apk del --purge .build-dep ; \
        rm -rf /var/cache/* \
               /var/tmp/* \
               /tmp/* \
               /usr/lib/python* \
               /root/.cache
