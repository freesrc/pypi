#!/usr/bin/env python3

import os
import requests
import logging

GITLAB_API_URL = os.getenv('CI_API_V4_URL')
GITLAB_API_TOKEN = os.getenv('GL_PRIVATE_TOKEN')
GITLAB_PROJECT_ID = os.getenv('CI_PROJECT_ID')
GITLAB_PROGECT_DIR = os.getenv('CI_PROJECT_DIR')

def get_pckages_list(project_id, timeout=30):
    try:
        resp = requests.get(
            url=f"{GITLAB_API_URL}/projects/{project_id}/packages?per_page=1000&package_type=pypi",
            headers={
                "PRIVATE-TOKEN": f"{GITLAB_API_TOKEN}"
            },
            timeout=timeout
        )

        resp.raise_for_status()
        return resp.json()
    except requests.Timeout:
        logging.error("Connection timed out.")
    except Exception as e:
        logging.error(f"Error: {e}")

    return None

def get_package_files(project_id, package_id, timeout=30):
    logging.info(f"Get list of package {package_id} files.")
    try:
        resp = requests.get(
            url=f"{GITLAB_API_URL}/projects/{project_id}/packages/{package_id}/package_files",
            headers={
                "PRIVATE-TOKEN": f"{GITLAB_API_TOKEN}"
            },
            timeout=timeout
        )

        resp.raise_for_status()
        return resp.json()
    except requests.Timeout:
        logging.error("Connection timed out.")
    except Exception as e:
        logging.error(f"Error: {e}")

    return None

def del_package_file(project_id, package_id, file_id, timeout=30):
    try:
        resp = requests.delete(
            url=f"{GITLAB_API_URL}/projects/{project_id}/packages/{package_id}/package_files/{file_id}",
            headers={
                "PRIVATE-TOKEN": f"{GITLAB_API_TOKEN}"
            },
            timeout=timeout
        )
        resp.raise_for_status()
    except requests.Timeout:
        logging.error("Connection timed out.")
    except Exception as e:
        logging.error(f"Error: {e}")

def get_packages_files(project_id):
    packages_files = []
    packages_list = get_pckages_list(project_id)

    for package in packages_list:
        files = get_package_files(project_id, package.get('id'))
        for file in files:
            file_info = {
                "pkg_id": package.get('id'),
                "pkg_name": package.get('name'),
                "file_id": file.get('id'),
                "file_name": file.get('file_name')
            }
            logging.info(f"File info: {file_info}")

            packages_files.append(file_info)

    return packages_files

def main():
    logging.info("Check packages.")
    logging.info("Get list of existad packages.")
    exists_files = get_packages_files(GITLAB_PROJECT_ID)
    logging.info("Get list of packages to upload.")
    upload_files = next(os.walk(GITLAB_PROGECT_DIR + '/rebuild'), (None, None, []))[2]

    for pkg_file in exists_files:
        if pkg_file['file_name'] in upload_files:
            logging.info(f"File {pkg_file['file_name']} for package {pkg_file['pkg_name']} alredy exists. Removing.")
            del_package_file(GITLAB_PROJECT_ID, pkg_file['pkg_id'], pkg_file['file_id'])

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    main()
